"use strict";

class BookController {
  constructor(app) {
    this.Book = app.models.Book;
  }

  async createBooks(members) {
    return await this.Book.create([
      { title: "asdf", memberId: members[0].id },
      { title: "fdsa" }
    ]).catch(err => {
      throw err;
    });
  }
}

module.exports = BookController;
