"use strict";

class MemberController {
  constructor(app) {
    this.Member = app.models.Member;
  }

  async createMembers() {
    return await this.Member.create([
      { email: "asd@asd.com", password: "123456" },
      { email: "das@dsa.com", password: "321654" }
    ]).catch(err => {
      throw err;
    });
  }
}

module.exports = MemberController;
