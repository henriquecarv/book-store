"use strict";

class RoleController {
  constructor(app) {
    this.Role = app.models.Role;
    this.RoleMapping = app.models.RoleMapping;
  }

  createRole(roleName) {
    return this.Role.create({ name: roleName }).catch(err => {
      throw err;
    });
  }

  setMemberRole(role, member) {
    role.principals
      .create({
        principalType: this.RoleMapping.USER,
        principalId: member.id
      })
      .catch(err => {
        throw err;
      });
  }
}

module.exports = RoleController;
