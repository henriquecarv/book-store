"use strict";

const BookController = require("./../../controllers/BookController");
const MemberController = require("./../../controllers/MemberController");
const RoleController = require("./../../controllers/RoleController");

module.exports = async app => {
  const memberController = new MemberController(app);
  const bookController = new BookController(app);
  const roleController = new RoleController(app);

  const members = await memberController.createMembers();
  const books = await bookController.createBooks(members);
  const adminRole = await roleController.createRole("admin");
  const customerRole = await roleController.createRole("customer");

  await roleController.setMemberRole(adminRole, members[1]);
  await roleController.setMemberRole(customerRole, members[0]);

  console.log("Books:", books);
};
